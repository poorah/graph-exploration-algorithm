#include <bits/stdc++.h>

using namespace std ;

class Graph {
    public:
        // properties :
        map<int, bool> visited;
        map<int, list<int>> adj;
        //methods :
        void addEdge(int v, int w);
        void Explore(int v);
};

void Graph::addEdge(int v, int w) {
    //TODO
    adj[v].push_back(w);
    adj[w].push_back(v);
}

void Graph::Explore(int v) {
    //TODO
    visited[v] = true ;
    cout << v << " -> " ;
    list<int>::iterator i;

    for(i = adj[v].begin(); i != adj[v].end(); i++) {
        if(!(visited[*i])) {
            Explore(*i) ;
        }
    }
}

int main() {

    Graph g ;
    
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    //g.addEdge(3, 1);
    g.addEdge(4, 5);
    g.addEdge(5, 6);
    //g.addEdge(6, 4);

    g.Explore(5) ;
    cout << "end" ;
    
    return 0;
}
